<?php namespace Premmerce\Filter\Seo;

use Premmerce\Filter\Filter\Container;
use Premmerce\Filter\Filter\Filter;
use Premmerce\Filter\Filter\Items\Types\FilterInterface;
use Premmerce\Filter\Filter\Items\Types\TaxonomyFilter;
use Premmerce\Filter\Seo\Sitemap\SeoFilterSitemapProvider;

class SeoListener
{

    /**
     * @var SeoModel
     */
    private $seoModel;

    /**
     * @var array
     */
    private $rule;

    /**
     * @var array
     */
    private $settings;

    /**
     * @var array
     */
    private $formatted = [];
    /**
     * @var array
     */
    private $formattedAttributesReplacements = [];

    /**
     * SeoListener constructor.
     */
    public function __construct()
    {
        add_action('woocommerce_product_query', [$this, 'findRule']);

        add_action('premmerce_filter_rule_found', [$this, 'registerActionsForRule']);

        add_action('premmerce_filter_rule_found', [$this, 'removeSeoActionsForRule']);

        add_action('premmerce_filter_rule_not_found', [$this, 'ruleNotFound']);

        add_filter('wpseo_sitemaps_providers', [$this, 'addSitemap']);

        $clearSitemapActions = [
            'save_post_product',
            'premmerce_filter_seo_rule_created',
            'premmerce_filter_seo_rule_updated',
            'premmerce_filter_seo_bulk_rules_removed',
            'premmerce_filter_seo_bulk_rules_updated',
            'update_option_premmerce_filter_seo_settings',
        ];

        foreach ($clearSitemapActions as $action) {
            add_action($action, [$this, 'clearSiteMap']);
        }

        $this->settings = get_option('premmerce_filter_seo_settings', []);

        $this->settings = array_merge(
            [
                "use_default_settings" => null,
                "h1" => null,
                "title" => null,
                "meta_description" => null,
                "description" => null
            ],
            $this->settings
        );

        $this->seoModel = new SeoModel();
    }

    /**
     * Add rules sitemap to YOAST SEO
     *
     * @param $array
     *
     * @return array
     */
    public function addSitemap($array)
    {
        $array[] = new SeoFilterSitemapProvider($this->seoModel);

        return $array;
    }

    /**
     * Clear rules sitemap
     */
    public function clearSiteMap()
    {
        if (class_exists('WPSEO_Sitemaps_Cache')) {
            \WPSEO_Sitemaps_Cache::clear(['filter_seo_rule']);
        }
    }

    /**
     * Find Rule by path
     */
    public function findRule()
    {
		
        if (is_product_category() && is_filtered() && !is_paged()) {
			
            $path = parse_url($_SERVER['REQUEST_URI'])['path'];
			
            $rule = $this->seoModel
                ->where(['path' => trim($path, '/'), 'enabled' => 1])
                ->returnType(SeoModel::TYPE_ROW)
                ->limit(1)
                ->get();

            if (!$rule && !empty($this->settings['use_default_settings'])) {
                $rule = $this->settings;
            }

            if (is_array($rule)) {
               	do_action('premmerce_filter_rule_found', ['rule' => $rule]);
				
            } else {
                do_action('premmerce_filter_rule_not_found', ['path' => $path]);
            }
        }


    }


    /**
     * Register actions for found rule
     *
     * @param array $args
     */
    public function registerActionsForRule($args)
    {
        $this->rule = $args['rule'];

        add_filter('pre_get_document_title', [$this, 'documentTitle'], 30);

        add_filter('woocommerce_page_title', [$this, 'pageTitle']);
		
		add_filter('woocommerce_page_titleH1', [$this, 'pageTitleH1']);

        add_action('wp_head', [$this, 'addMetaDescription'], 1);
		
		add_action('wp_head', [$this, 'addCanonical'], 1);

        remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
        add_action('woocommerce_archive_description', [$this, 'addDescription'], 20);
		
    }

    /**
     * Replace term description
     */
    public function addDescription()
    {
        $description = $this->parseVariables($this->rule['description']);
		
        if ($description) {
            printf('<div class="term-description">%s</div>', wc_format_content($description));
        }
    }

    /**
     * Add meta description
     */
    public function addMetaDescription()
    {
        $metaDescription = $this->escape($this->parseVariables($this->rule['meta_description']));

        printf("<meta name='description' content='%s'>", $metaDescription);
    }

    /**
     * Replace document title
     * @return string
     */
    public function documentTitle()
    {
        return $this->escape($this->parseVariables($this->rule['title']));
    }

    /**
     * Replace H1
     * @return string
     */
    public function pageTitle()
    {
        return $this->parseVariables($this->rule['h1']);
    }
	 public function pageTitleH1()
    {
        echo $this->parseVariables($this->rule['h1']);
    }


    /**
     * Add canonical if rule not found
     */
    public function ruleNotFound()
    {
        if (!defined('WPSEO_VERSION')) {
            add_action('wp_head', [$this, 'addCanonical']);
        }
    }

    /**
     * Ad canonical with term url
     */
    public function addCanonical()
    {
		
        $term = get_queried_object();

        if ($term instanceof \WP_Term) {
            $canonical = apply_filters('premmerce_filter_canonical_url', get_term_link($term), $term);
			
            if (is_string($canonical)) {
				if(!empty($this->rule['canonicalurl'])){
					printf('<link rel="canonical" href="%s" />', $this->rule['canonicalurl']);
				}
				else{
					printf('<link rel="canonical" href="%s" />', esc_url($canonical, null, null));
				}
                
				
            }
        }
		
    }

    /**
     * Remove canonical if rule found
     */
    public function removeSeoActionsForRule()
    {
        if (defined('WPSEO_VERSION')) {
            remove_action('wpseo_head', [\WPSEO_Frontend::get_instance(), 'canonical'], 20);
            remove_action('wpseo_head', [\WPSEO_Frontend::get_instance(), 'metadesc'], 6);
        }
		
    }

    /**
     * Parse variables in SEO text
     *
     * @param $text
     *
     * @return string
     */
    public function parseVariables($text)
    {
        if (empty($this->formatted)) {
            $items = Container::getInstance()->getItemsManager()->getItems();
            $attributeItemsForSeo = array_filter($items, [$this, 'canBeUsedForSeoAttributeText']);
            $this->formatted['attributes'] = $this->formatAttributes($attributeItemsForSeo);
            $this->formattedAttributesReplacements = $this->formatIndividualAttributes($attributeItemsForSeo);

            $this->formatted['brands'] = $this->formatBrands($items);
            $this->formatted['prices'] = Container::getInstance()->getPriceQuery()->getPrices();
        }


        global $wp_query;

        $replacements = [
            '{name}' => get_queried_object()->name,
            '{count}' => $wp_query->found_posts,
            '{description}' => get_queried_object()->description,
            '{attributes}' => $this->formatted['attributes'],
            '{brands}' => $this->formatted['brands'],
            '{min_price}' => $this->formatted['prices']['min_selected'],
            '{max_price}' => $this->formatted['prices']['max_selected'],
        ];
        $replacements = array_merge($replacements, $this->formattedAttributesReplacements);;
        $result = strtr($text, $replacements);

        return trim($result);
    }

    /**
     * @param FilterInterface[] $filters
     * @return array
     */
    public function formatIndividualAttributes(array $filters)
    {
        $replacements = [];

        foreach ($filters as $filter) {
            $attributeNameVariable = '{attribute_name_' . $filter->getSlug() . '}';
            $replacements[$attributeNameVariable] = $filter->getLabel();

            $activeItems = array_column($filter->getActiveItems(), 'title', 'id');

            $attributeValueVariable = '{attribute_value_' . $filter->getSlug() . '}';
            $replacements[$attributeValueVariable] = implode(', ', array_values($activeItems));

            $terms = array_column($filter->getItems(), 'term_id', 'name');
            foreach ($activeItems as $id => $title) {
                $tid = isset($terms[$title]) ? $terms[$title] : $id;
                $replacements['{attribute_value_' . $filter->getSlug() . '_' . $tid . '}'] = $title;
            }
        }

        return $replacements;
    }

    /**
     * @param FilterInterface[] $items
     *
     * @return string
     */
    protected function formatBrands($items)
    {
        foreach ($items as $item) {
            if ($item->getId() === 'product_brand') {
                $item->init();
                $active = $item->getActiveItems();
                $names = array_column($active, 'title');

                if (count($names)) {
                    return implode(', ', $names);
                }
            }
        }
    }

    /**
     * Format attributes for seo text variable
     *
     * @param $attributes
     *
     * @return string
     */
    protected function formatAttributes($attributes)
    {
        $attributeStrings = [];
        /** @var FilterInterface $attribute */
        foreach ($attributes as $attribute) {
            $attribute->init();
            $items = $attribute->getActiveItems();
            $names = array_column($items, 'title');
            if (count($names)) {
                $attributeStrings[] = $attribute->getLabel() . ' (' . implode(', ', $names) . ')';
            }
        }

        return apply_filters('premmerce_filter_seo_format_attributes', implode(', ', $attributeStrings), $attributes);
    }

    /**
     * @param FilterInterface $attribute
     *
     * @return bool
     */
    protected function canBeUsedForSeoAttributeText(FilterInterface $attribute)
    {
        return $attribute->isActive()
            && $attribute->getType() !== 'slider'
            && (taxonomy_is_product_attribute($attribute->getId()) || $attribute->getSlug() === 'product_brand');
    }

    /**
     * Escape text for tag attribute
     *
     * @param $text
     *
     * @return string
     */
    private function escape($text)
    {
        return esc_attr(wp_strip_all_tags(stripslashes($text)));
    }
}
