<?php namespace Premmerce\Filter\Admin\Tabs;

use Premmerce\Filter\Admin\Tabs\Base\TabInterface;
use Premmerce\Filter\Seo\RulesGenerator;
use Premmerce\Filter\Seo\RulesTable;
use Premmerce\Filter\Seo\SeoModel;
use Premmerce\Filter\Seo\WPMLHelper;
use Premmerce\SDK\V2\FileManager\FileManager;
use Premmerce\SDK\V2\Notifications\AdminNotifier;

class SeoRules implements TabInterface
{

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var SeoModel
     */
    private $model;

    /**
     * @var AdminNotifier
     */
    private $notifier;

    /**
     * @var RulesGenerator
     */
    private $generator;

    const KEY_UPDATE_PATHS = 'premmerce_filter_update_paths';

    /**
     * SeoRules constructor.
     *
     * @param FileManager $fileManager
     * @param AdminNotifier $notifier
     */
    public function __construct(FileManager $fileManager, AdminNotifier $notifier)
    {
        $this->fileManager = $fileManager;
        $this->model = new SeoModel();
        $this->notifier = $notifier;
        $this->generator = new RulesGenerator($this->model);
    }

    /**
     * Register hooks
     */
    public function init()
    {
        add_action('admin_post_premmerce_filter_seo_create', [$this, 'create']);
        add_action('admin_post_premmerce_filter_seo_update', [$this, 'update']);
        add_action('wp_ajax_get_taxonomy_terms', [$this, 'getTaxonomyTerms']);
        add_action('wp_ajax_premmerce_filter_seo_generate_next', [$this, 'generateNext']);
        add_action('wp_ajax_premmerce_filter_seo_update_next', [$this, 'updateNext']);
    }


    /**
     * Create rule
     */
    public function create()
    {
        $result = $this->model->save($_POST);

        if ($result === null) {
            $this->notifier->flash(__('Rule does not contains products', 'premmerce-filter'), AdminNotifier::WARNING);
        } elseif ($result instanceof \WP_Error) {
            $this->notifier->flash($result->get_error_message(), AdminNotifier::ERROR);
        } else {
            $this->notifier->flash(__('Rule created', 'premmerce-filter'), AdminNotifier::SUCCESS);
        }

        $this->redirectBack();
    }

    /**
     * Update rule
     */
    public function update()
    {
		
        $id = $this->model->save($_POST);

        if ($id === null) {
            $this->notifier->flash(__('Rule does not contains products', 'premmerce-filter'), AdminNotifier::WARNING);
        } elseif ($id instanceof \WP_Error) {
            $this->notifier->flash($id->get_error_message(), AdminNotifier::ERROR);
        } else {
            $this->notifier->flash(__('Rule updated', 'premmerce-filter'), AdminNotifier::SUCCESS);
        }

        $this->redirectBack();
    }

    /**
     * Ajax get terms
     */
    public function getTaxonomyTerms()
    {
        $terms = get_terms(
            [
                'taxonomy' => $_POST['taxonomy'],
                'hide_empty' => false,
            ]
        );

        if ($terms instanceof \WP_Error) {
            $terms = [];
        }

        $output = ['results' => []];

        foreach ($terms as $term) {
            list($id, $text, $slug) = array_values((array)$term);
            $output['results'][] = ['id' => $id, 'text' => $text, 'slug' => $slug, 'taxonomy' => $term->taxonomy];
        }

        echo json_encode($output);
        wp_die();
    }

    /**
     * Render tab content
     */
    public function render()
    {
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

        switch ($action) {
            case 'edit':
                $this->renderEdit();
                break;
            case 'generate_rules':
                $this->renderGenerate();
                break;
            case 'update_paths':
                $this->startUpdatePathsProgress();
                break;
            case 'generation_progress':
                $this->startGenerationProgress();
                break;
            default:
                $this->renderList();
                break;
        }
    }


    /**
     * Generate next rule
     */
    public function generateNext()
    {
        $count = $this->generator->next();

        if (!$count) {
            $this->notifier->flash(__('Generation completed', 'premmerce-filter'), AdminNotifier::SUCCESS);
        }

        $this->handleProgress($count);
    }

    /**
     * Update next rule
     */
    public function updateNext()
    {
        $rules = get_option(self::KEY_UPDATE_PATHS, []);

        $ruleId = array_shift($rules);

        update_option(self::KEY_UPDATE_PATHS, $rules);

        $rule = $this->model->returnType(SeoModel::TYPE_ROW)->where(['id' => $ruleId])->get(['id', 'term_id']);

        if ($rule) {
            $this->model->update(
                $rule['id'],
                [
                    'terms' => $this->model->getTerms($rule['id']),
                    'term_id' => $rule['term_id']
                ]
            );
        }

        $count = count($rules);

        if (!$count) {
            delete_option(self::KEY_UPDATE_PATHS);
            $this->notifier->flash(__('Rules are updated', 'premmerce-filter'), AdminNotifier::SUCCESS);
        }

        $this->handleProgress($count);
    }


    /**
     * @param $count
     */
    public function handleProgress($count)
    {
        if ($count > 0) {
            echo json_encode(['status' => 'next']);
            wp_die();
        }
        echo json_encode(['status' => 'complete']);
        wp_die();
    }

    /**
     * Page with update progress bar
     */
    public function startUpdatePathsProgress()
    {
        $join = WPMLHelper::joinTermWithWPMLCurrentTranslation('r.term_id');

        $rules = $this->model->alias('r')->joinRaw($join)->returnType(SeoModel::TYPE_COLUMN)->get(['id']);
        $max = count($rules);

        if (!$max) {
            $this->redirectBack();
        }

        update_option(self::KEY_UPDATE_PATHS, $rules);


        $action = 'premmerce_filter_seo_update_next';
        $complete = menu_page_url('premmerce-filter-admin', false) . '&tab=seo';

        $this->fileManager->includeTemplate(
            'admin/seo/generate-progress.php',
            compact('max', 'action', 'complete')
        );
    }

    /**
     * Page with generation progress bar
     */
    public function startGenerationProgress()
    {
        $results = $this->generator->start($_REQUEST);

        $max = count($results);


        if (!$max) {
            $this->redirectBack();
        }


        $action = 'premmerce_filter_seo_generate_next';
        $complete = menu_page_url('premmerce-filter-admin', false) . '&tab=seo';

        $this->fileManager->includeTemplate(
            'admin/seo/generate-progress.php',
            compact('max', 'action', 'complete')
        );
    }

    /**
     * Bulk generation
     */
    public function renderGenerate()
    {
        $attributes = $this->getAttributes();

        $categories = get_terms(
            [
                'taxonomy' => 'product_cat',
                'hide_empty' => false,
                'fields' => 'id=>name'
            ]
        );

        $this->fileManager->includeTemplate(
            'admin/seo/generate-rules.php',
            compact('attributes', 'categories')
        );
    }

    /**
     * Render edit page
     *
     */
    public function renderEdit()
    {
        $id = $_GET['id'];
        $rule = $this->model->find($id);

        $categoriesDropDownArgs = $this->getCategoryDropdownArgs();

        $categoriesDropDownArgs['selected'] = $rule['term_id'];

        $attributes = $this->getAttributes();

        $this->fileManager->includeTemplate(
            'admin/seo/form.php',
            compact('rule', 'categoriesDropDownArgs', 'attributes')
        );
    }

    /**
     * Render rules list
     */
    public function renderList()
    {
        $categoriesDropDownArgs = $this->getCategoryDropdownArgs();
        $attributes = $this->getAttributes();

        $table = new RulesTable($this->fileManager, $this->model);

        $rule = [
            'id' => '',
            'term_id' => '',
            'path' => '',
            'h1' => '',
            'title' => '',
            'meta_description' => '',
            'description' => '',
            'enabled' => 1,
			'noindex' => 1,
			'canonicalurl' => '',
            'data' => null,
        ];

        $this->fileManager->includeTemplate(
            'admin/tabs/seo.php',
            [
                'categoriesDropDownArgs' => $categoriesDropDownArgs,
                'attributes' => $attributes,
                'rulesTable' => $table,
                'rule' => $rule,
                'fm' => $this->fileManager,
            ]
        );
    }

    /**
     * Tab label
     *
     * @return string
     */
    public function getLabel()
    {
        return __('SEO Rules', 'premmerce-filter');
    }

    /**
     * Tab name
     *
     * @return string
     */
    public function getName()
    {
        return 'seo';
    }

    /**
     * Is tab valid
     *
     * @return bool
     */
    public function valid()
    {
        return true;
    }

    /**
     * Arguments for category select
     * @return array
     */
    private function getCategoryDropdownArgs()
    {
        $categoriesDropDownArgs = [
            'hide_empty' => 0,
            'hide_if_empty' => false,
            'taxonomy' => 'product_cat',
            'name' => 'term_id',
            'orderby' => 'name',
            'hierarchical' => true,
            'show_option_none' => false,
        ];

        $categoriesDropDownArgs = apply_filters(
            'taxonomy_parent_dropdown_args',
            $categoriesDropDownArgs,
            'product_cat',
            'new'
        );

        return $categoriesDropDownArgs;
    }

    /**
     * Get attributes for term selects
     *
     * @return array
     */
    private function getAttributes()
    {
        $wcAttributes = wc_get_attribute_taxonomies();

        $attributes = [];
        foreach ($wcAttributes as $attribute) {
            $attributes['pa_' . $attribute->attribute_name] = $attribute->attribute_label;
        }

        if (taxonomy_exists('product_brand')) {
            $brandTaxonomy = get_taxonomy('product_brand');
            $attributes[$brandTaxonomy->name] = $brandTaxonomy->label;
        }

        return $attributes;
    }

    /**
     * Redirect to previous page
     */
    private function redirectBack()
    {
        wp_redirect($_SERVER['HTTP_REFERER']);
        die;
    }

}
