<?php
/**
 * Plugin Name: Furnitureroots - Base language Change Plugin
 * Plugin URI: https://furnitureroots.com
 * Description: This change the base language hreflang
 * Version: 1.0
 * Author: Akshay Prabhale
 * Author URI: https://akshayprabhale.com
 * License: GPL2
 */
 
/*  Copyright YEAR  Akshay_Prabhale  (email : akshay.prabhale91@gmail.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Start writing code after this line!
add_filter('trp_hreflang', 'trpc_change_hreflang', 10, 2 );
 function trpc_change_hreflang( $hreflang, $language ){
     if ($language == 'en_US'){  // language code that you want to alter
 
         // the modified hreflang code you want to display in the page 
         $hreflang = 'en';    
     }
     return $hreflang;
 }