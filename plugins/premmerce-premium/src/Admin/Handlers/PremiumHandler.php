<?php namespace Premmerce\Premmerce\Admin\Handlers;

use Premmerce\Premmerce\Api\PluginApi;
use Premmerce\SDK\V2\FileManager\FileManager;

class PremiumHandler{


	/**
	 * @var FileManager
	 */
	private $fileManager;
	/**
	 * @var PluginApi
	 */
	private $pluginApi;

	private $imagecmsLocale = ['ru', 'ru_RU', 'uk', 'uk_UA',];

	public function __construct(PluginApi $pluginApi, FileManager $fileManager){
		$this->fileManager = $fileManager;

		$this->pluginApi = $pluginApi;
	}

	public function addActions(){
		add_action('login_enqueue_scripts', function(){
			$style = $this->isImageCMS()? 'login-imagecms.css' : 'login-premmerce.css';
			wp_enqueue_style('premmerce_login', $this->fileManager->locateAsset('admin/css/' . $style));
		});

		add_filter('login_headerurl', function(){
			$link = $this->isImageCMS()? 'https://imagecms.net' : 'https://premmerce.com';

			return $link;
		});

		add_action('admin_enqueue_scripts', [$this, 'enqueueToolbarScripts']);
		add_action('wp_enqueue_scripts', [$this, 'enqueueToolbarScripts']);

	}

	public function enqueueToolbarScripts(){
		$file = $this->isImageCMS()? 'imagecms-toolbar.css' : 'premmerce-toolbar.css';
		wp_enqueue_style('premmerce-toolbar', $this->fileManager->locateAsset('admin/css/' . $file));
	}

	public function isImageCMS(){
		return in_array(get_user_locale(), $this->imagecmsLocale);
	}

	public function addAdminActions(){

		add_action('wp_before_admin_bar_render', [$this, 'replaceToolbarWpItem']);

		add_action('wp_ajax_enable_ecommerce_menu', [$this, 'enableMenu']);

		add_action('wp_ajax_enable_ecommerce_menu', [$this, 'enableMenu']);
	}

	public function replaceToolbarWpItem(){
		global /** @var \WP_Admin_Bar $wp_admin_bar */
		$wp_admin_bar;

		$about = $wp_admin_bar->get_node('about');

		$docs = $wp_admin_bar->get_node('documentation');

		$wp_admin_bar->remove_node('about');
		$wp_admin_bar->remove_node('wporg');
		$wp_admin_bar->remove_node('support-forums');
		$wp_admin_bar->remove_node('feedback');
		$wp_admin_bar->remove_node('documentation');

		$wp_admin_bar->add_node([
			'id'     => 'premmerce_com',
			'title'  => __('Premmerce.com', 'premmerce'),
			'parent' => 'wp-logo',
			'href'   => __('https://premmerce.com', 'premmerce'),
		]);

		$wp_admin_bar->add_node([
			'id'     => 'premmerce_docs',
			'title'  => __('Premmerce Documentation', 'premmerce'),
			'parent' => 'wp-logo',
			'href'   => __('https://premmerce.com/premmerce-main-plugin-woocommerce-plugins-bundle/', 'premmerce'),
		]);
		$about = [
			"id"     => $about->id,
			"title"  => $about->title,
			"parent" => $about->parent,
			"href"   => $about->href,
			"group"  => $about->group,
			"meta"   => $about->meta,
		];

		$docs = [
			"id"     => $docs->id,
			"title"  => __('WordPress Documentation', 'premmerce'),
			"parent" => 'wp-logo',
			"href"   => $docs->href,
			"group"  => $docs->group,
			"meta"   => $docs->meta,
		];

		$wp_admin_bar->add_node($about);
		$wp_admin_bar->add_node($docs);

	}
}