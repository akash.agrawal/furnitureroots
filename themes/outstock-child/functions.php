<?php 
add_action( 'wp_enqueue_scripts', 'outstock_child_scripts_styles', 15 );
function outstock_child_scripts_styles(){
	global $outstock_opt;
	if($outstock_opt['enable_less']){
		$params = array(
			// less variables
		);
		if( function_exists('compileLess') ){
			compileLess('child-theme.less', 'child-theme-less.css', $params);
		}
	}
	if(file_exists(get_stylesheet_directory() . '/css/child-theme.css')){
		wp_enqueue_style( 'child-theme-less', get_stylesheet_directory_uri() . '/css/child-theme-less.css' );
	}
	wp_enqueue_style( 'outstock-child-theme', get_stylesheet_directory_uri() . '/css/child-theme-css.css' );
	wp_enqueue_style( 'outstock-child-style', get_stylesheet_directory_uri() . '/style.css' );
}

/** Disable Ajax Call from WooCommerce */
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11); 
function dequeue_woocommerce_cart_fragments() { if (is_front_page()) wp_dequeue_script('wc-cart-fragments'); }


add_filter( 'autoptimize_filter_imgopt_should_lazyload', '__return_false', 11, 0);

/*
	Get Script and Style IDs
	Adds inline comment to your frontend pages
	View source code near the <head> section
	Lists only properly registered scripts
	@ https://digwp.com/2018/08/disable-script-style-added-plugins/
*/
function shapeSpace_inspect_script_style() {
	
	global $wp_scripts, $wp_styles;
	
	echo "\n" .'<!--'. "\n\n";
	
	echo 'SCRIPT IDs:'. "\n";
	
	foreach($wp_scripts->queue as $handle) echo $handle . "\n";
	
	echo "\n" .'STYLE IDs:'. "\n";
	
	foreach($wp_styles->queue as $handle) echo $handle . "\n";
	
	echo "\n" .'-->'. "\n\n";
	
}
add_action('wp_print_scripts', 'shapeSpace_inspect_script_style');









// disable stylesheet (example)
function js_composer_front() {
	
	wp_dequeue_style('js_composer_front');
// 	wp_deregister_style('js_composer_front');
	
		wp_dequeue_script('nbcpf-intlTelInput-script');
wp_dequeue_script('nbcpf-countryFlag-script');
wp_dequeue_script('google-maps');
wp_dequeue_style('nbcpf-intlTelInput-style');
wp_dequeue_style('nbcpf-countryFlag-style');
	
}
add_action('wp_enqueue_scripts', 'js_composer_front', 100);




// function remove_category( $string, $type )
// { 
//         if ( $type != 'single' && $type == 'category' && ( strpos( $string, 'home-page' ) !== false ) )
//         {
//             $url_without_category = str_replace( "/home-page/", "/", $string );
//             return trailingslashit( $url_without_category );
//         }
//     return $string;
// }
 
// add_filter( 'user_trailingslashit', 'remove_category', 100, 2);





/** Disable All WooCommerce  Styles and Scripts Except Shop Pages*/
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_styles_scripts', 99 );
function dequeue_woocommerce_styles_scripts() {
if ( function_exists( 'is_woocommerce' ) ) {
if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
# Styles
wp_dequeue_style( 'woocommerce-general' );
wp_dequeue_style( 'woocommerce-layout' );
wp_dequeue_style( 'woocommerce-smallscreen' );
wp_dequeue_style( 'woocommerce_frontend_styles' );
wp_dequeue_style( 'woocommerce_fancybox_styles' );
wp_dequeue_style( 'woocommerce_chosen_styles' );
wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
# Scripts
wp_dequeue_script( 'wc_price_slider' );
wp_dequeue_script( 'wc-single-product' );
wp_dequeue_script( 'wc-add-to-cart' );
wp_dequeue_script( 'wc-cart-fragments' );
wp_dequeue_script( 'wc-checkout' );
wp_dequeue_script( 'wc-add-to-cart-variation' );
wp_dequeue_script( 'wc-single-product' );
wp_dequeue_script( 'wc-cart' );
wp_dequeue_script( 'wc-chosen' );
wp_dequeue_script( 'woocommerce' );
wp_dequeue_script( 'prettyPhoto' );
wp_dequeue_script( 'prettyPhoto-init' );
wp_dequeue_script( 'jquery-blockui' );
wp_dequeue_script( 'jquery-placeholder' );
wp_dequeue_script( 'fancybox' );
wp_dequeue_script( 'jqueryui' );
}
}
}





function remove_nofollow($string) {
	$string = str_ireplace(' rel="nofollow"', '', $string);
	return $string;
}

add_filter('the_content', 'remove_nofollow');
add_filter('wp_nav_menu_items', 'remove_nofollow');

/*add_filter( 'rank_math/frontend/robots', function( $robots ) {
	unset( $robots['index'] );
	unset( $robots['follow'] );
	return $robots;
});*/
/**
 * Allow HTML in term (category, tag) descriptions
 */
foreach ( array( 'pre_term_description' ) as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
	if ( ! current_user_can( 'unfiltered_html' ) ) {
		add_filter( $filter, 'wp_filter_post_kses' );
	}
}
 
foreach ( array( 'term_description' ) as $filter ) {
	remove_filter( $filter, 'wp_kses_data' );
}

function km_add_unfiltered_html_capability_to_editors( $caps, $cap, $user_id ) {

 if ( 'unfiltered_html' === $cap && user_can( $user_id, 'editor' ) ) {

 $caps = array( 'unfiltered_html' );

 }

 return $caps;
}
add_filter( 'map_meta_cap', 'km_add_unfiltered_html_capability_to_editors', 1, 3 );
function change_mce_options($init){
    $init["forced_root_block"] = false;
    $init["force_br_newlines"] = true;
    $init["force_p_newlines"] = false;
    $init["convert_newlines_to_brs"] = true;
    return $init;       
}


/**
 * Filter to modify Rank Math product schema data
 * Replace $schema_type with schema name like article, review, etc.
 * @param array $entity Snippet Data
 * @return array
 */
add_filter( "rank_math/snippet/rich_snippet_product_entity", function( $entity ) {
	unset($entity['offers']['price']);
	
	return $entity;
});



function add_noprefix_attribute($link, $handle) {
    if( $handle === 'google-fonts' ) {
        $link = str_replace( '/>', 'data-noprefix />', $link );
    }
    return $link;
}
/**
 * Filter to modify schema data.
 *
 * @param array $entity Snippet Data
 * @return array
 */
add_filter( 'rank_math/snippet/rich_snippet_product_entity', function( $entity ) {
   
	$entity['gtin13'] = str_replace('-', '',$entity['sku']);

	return $entity;
});


add_filter( 'rank_math/snippet/rich_snippet_product_entity', function( $entity ) {
	if ( isset( $entity['isRelatedTo'] ) ) {
		unset( $entity['isRelatedTo'] );
	}
	return $entity;
});


function bamboo_request($query_string )
{
    if( isset( $query_string['page'] ) ) {
        if( ''!=$query_string['page'] ) {
            if( isset( $query_string['name'] ) ) {
                unset( $query_string['name'] );
            }
        }
    }
    return $query_string;
}
add_filter('request', 'bamboo_request');



add_action('pre_get_posts','bamboo_pre_get_posts');
function bamboo_pre_get_posts( $query ) {
   if( $query->is_main_query() && !$query->is_feed() && !is_admin() ) {
       $query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) );
   }
}


add_filter( 'rank_math/snippet/rich_snippet_product_entity', function( $entity ) {
$entity['brand'] = 'FurnitureRoots';
return $entity;
});

// function rocket_lazyload_exclude_class( $attributes ) {
// 	$attributes[] = 'class="vc_single_image-img';

// 	return $attributes;
// }
// add_filter( 'rocket_lazyload_excluded_attributes', 'rocket_lazyload_exclude_class' );


/**
 * Disable WooCommerce block styles (front-end).
 */
function slug_disable_woocommerce_block_styles() {
  wp_dequeue_style( 'wc-block-style' );
}
add_action( 'wp_enqueue_scripts', 'slug_disable_woocommerce_block_styles' );



function fix_pagination_loop( $query ) {
	if( ($query->is_main_query() && ! is_admin()) || is_category() ) {
		$url = $_SERVER["REQUEST_URI"];
		$url_path = parse_url($url, PHP_URL_PATH);
		$basename = absint(pathinfo($url_path, PATHINFO_BASENAME));
		if($basename){
			$query->set( 'paged', $basename );
		}
	}
}
add_action( 'pre_get_posts', 'fix_pagination_loop' );





/* Remove the default WooCommerce 3 JSON/LD structured data */
function remove_output_structured_data() {
  remove_action( 'wp_footer', array( WC()->structured_data, 'output_structured_data' ), 10 ); // This removes structured data from all frontend pages
  remove_action( 'woocommerce_email_order_details', array( WC()->structured_data, 'output_email_structured_data' ), 30 ); // This removes structured data from all Emails sent by WooCommerce
}
add_action( 'init', 'remove_output_structured_data' );


add_filter('tiny_mce_before_init','change_mce_options');function add_custom_script() {

	
?>
<script>
$( document ).ready(function()
{ $('body > a').attr("rel", "");
});
</script>
<?php
}
add_action('wp_footer', 'add_custom_script');



add_filter( 'rank_math/frontend/canonical', function( $canonical ) {
	
	//return $canonical;
});

//add_filter('aioseop_canonical_url','remove_canonical_url', 10, 1);
//
//


function disable_emoji_feature() {
	
	// Prevent Emoji from loading on the front-end
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	// Remove from admin area also
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Remove from RSS feeds also
	remove_filter( 'the_content_feed', 'wp_staticize_emoji');
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji');

	// Remove from Embeds
	remove_filter( 'embed_head', 'print_emoji_detection_script' );

	// Remove from emails
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Disable from TinyMCE editor. Currently disabled in block editor by default
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );

	/** Finally, prevent character conversion too
         ** without this, emojis still work 
         ** if it is available on the user's device
	 */

	add_filter( 'option_use_smilies', '__return_false' );

}

function disable_emojis_tinymce( $plugins ) {
	if( is_array($plugins) ) {
		$plugins = array_diff( $plugins, array( 'wpemoji' ) );
	}
	return $plugins;
}

add_action('init', 'disable_emoji_feature');
add_filter( 'option_use_smilies', '__return_false' );


remove_filter ('the_content', 'wpautop'); 


function acf_wysiwyg_remove_wpautop() {
  // remove p tags //
  remove_filter('acf_the_content', 'wpautop' );
  // add line breaks before all newlines //
 
}

add_action('acf/init', 'acf_wysiwyg_remove_wpautop');





add_action('premmerce_product_active_filters_render', function ($data) {
    //var_dump($data);
});

add_action('wp_head', 'add_jsonld_head', 1);
function add_jsonld_head() {

	$pageterm = get_queried_object(); 
	if (have_rows('breadcrumbsfr', $pageterm)): 
?> 





	<script type="application/ld+json">
	{ 
		"@context": "http://schema.org", 
		"@type":  "BreadcrumbList", 
		"itemListElement": [
 		<?php  $rows = 0; while( have_rows('breadcrumbsfr', $pageterm ) ) : the_row(); $rows++; endwhile; while( have_rows('breadcrumbsfr', $pageterm ) ) : the_row(); $category = get_sub_field('name', $pageterm ); $url = get_sub_field('url', $pageterm );  ?> {
		   "@type": "ListItem",
		   "position": <?php echo get_row_index(); ?>,
		   "item": {
			    "@id": "<?php echo $url; ?>",
			    "name": "<?php echo $category; ?>"
	    	}
	  	}<?php if($rows !== get_row_index()) echo ','; endwhile; ?>
	  ]
	}
	</script>
<?php endif; }