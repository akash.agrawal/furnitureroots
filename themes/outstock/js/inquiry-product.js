jQuery(function ($) {
    /* Inquire JS START */
    if ($('body').hasClass('single-product') || $('body').hasClass('tax-product_cat')) {
        var cartdiv = '<div id="bottom-popup" class=""><div class="sticky-view-list"><span class="popup-text" style="margin-left: 20px;line-height: 40px;">Product added to enquiry list!</span><a href="/inquiry-list" class="list-btn border-white" style="">View List<span>0</span></a></div></div>';
        $('body').append(cartdiv);

        // ( POLYFiLL for include() function )
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
        // https://tc39.github.io/ecma262/#sec-array.prototype.includes
        if (!Array.prototype.includes) {
            Object.defineProperty(Array.prototype, 'includes', {
                value: function (valueToFind, fromIndex) {
                    if (this == null) {
                        throw new TypeError('"this" is null or not defined');
                    }
                    var o = Object(this);
                    var len = o.length >>> 0;
                    if (len === 0) {
                        return false;
                    }
                    var n = fromIndex | 0;
                    var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                    function sameValueZero(x, y) {
                        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
                    }
                    while (k < len) {
                        if (sameValueZero(o[k], valueToFind)) {
                            return true;product_cat
                        }
                        k++;
                    }
                    return false;
                }
            });
        }
        // POLYFiLL EnD

        function uniquevalue(value, index, self) {
            return self.indexOf(value) === index;
        }

        function get_unique_products() { //this function returns unique products in list
            if (typeof store.get('inquire_products') !== "undefined") {
                var temp = store.get('inquire_products').filter(uniquevalue);
                store.set('inquire_products', temp);
                return temp;
            } else {
                fr_hide_toast();
            }
        }

        var add_inq_btn = '.add-inquiry-btn';
        var remove_inq_btn = '.remove-inquiry-btn';
        var inquire_products = store.get('inquire_products');
        var product_list = [];
        var ref; // Passing referance for buttons.
        var btn;
        var new_product_list; // only for updating Inquiry List number as var product_list is uused to check if old data & new data matches on changing of tabs
        var that; // passing value of this into that so that we could use delecate. (AJAX Loading Items Fix)

        if (typeof inquire_products == 'undefined') {
            store.set('inquire_products', product_list);
            inquire_products = store.get('inquire_products');
        }

        function fr_show_toast() {
            $('#bottom-popup').addClass('active');
        }

        function fr_hide_toast() {
            $('#bottom-popup').removeClass('active');
        }

        //Inquiry List items number
        function update_view_list_number() {
            new_product_list = get_unique_products();
            if (new_product_list.length == 0) {
                fr_hide_toast();
            } else {
                fr_show_toast();
                $('.list-btn>span').text(new_product_list.length);
            }
        }

        function on_click_orange(ref) {
            ref = ref;
            btn = $(ref);
            btn.removeClass('active').hide();
            $(remove_inq_btn, btn.parent()).addClass('active').show();
        }

        function on_click_red(ref) {
            ref = ref;
            btn = $(ref);
            btn.removeClass('active').hide();
            $(add_inq_btn, btn.parent()).addClass('active').show();
        }

        // Add an item to list (onClick)
        function add_item(ref, post_id) {
            ref = ref;
            post_id = post_id;

            //Updating Store Data
            if (store.get('inquire_products').length != get_unique_products().lenth) {
                store.set('inquire_products', get_unique_products());
            }

            var new_inquire_products;
            var intersection = inquire_products.filter(element => store.get('inquire_products').includes(element));

            // If Current list == Store list
            if (inquire_products.length == store.get('inquire_products').length && inquire_products.length == intersection.length && store.get('inquire_products').length == intersection.length) {
                new_inquire_products = inquire_products;
                //Check if Product is already in Current List
                if (new_inquire_products.includes(post_id)) {
                    on_click_orange(ref);
                    update_view_list_number();
                } else {
                    new_inquire_products.push(post_id);
                    store.set('inquire_products', new_inquire_products);
                    inquire_products = store.get('inquire_products'); // For future add/remove oprations
                    simlar_products_check();
                    on_click_orange(ref);
                    update_view_list_number();
                }
            }

            // If Current list != Store list
            else {
                new_inquire_products = store.get('inquire_products');
                //Check if Product is already in Store List
                if (new_inquire_products.includes(post_id)) {
                    on_click_orange(ref);
                    update_view_list_number();
                } else {
                    new_inquire_products.push(post_id);
                    store.set('inquire_products', new_inquire_products);
                    inquire_products = store.get('inquire_products'); // For future add/remove oprations
                    simlar_products_check();
                    on_click_orange(ref);
                    update_view_list_number();
                }
            }
        }

        // // Remove an item from list (onClick)
        function remove_item(ref, post_id) {
            ref = ref;
            post_id = post_id;

            //Updating Store Data
            if (store.get('inquire_products').length != get_unique_products().lenth) {
                store.set('inquire_products', get_unique_products());
            }

            var saved_products = store.get('inquire_products');
            var index = $.inArray(post_id, saved_products);
            if (index > -1) {
                saved_products.splice(index, 1);
            }

            store.set('inquire_products', saved_products);
            inquire_products = store.get('inquire_products'); // For future add/remove oprations

            on_click_red(ref);
            update_view_list_number();
        }

        // Function for Single Product -> Similar Products Division
        function simlar_products_check() {
            if ($('.related_products_widget').length > 0) {
                var s_product_list = store.get('inquire_products');

                $(add_inq_btn, '.related_products_widget').each(function () {
                    var all_post_ids = $(this).attr('data-post-id');
                    ref = this;
                    if (s_product_list.indexOf(all_post_ids) > -1) {
                        on_click_orange(ref);
                    }
                });

                $(remove_inq_btn, '.related_products_widget').each(function () {
                    var all_post_ids = $(this).attr('data-post-id');
                    ref = this;
                    if (s_product_list.indexOf(all_post_ids) > -1) {
                        on_click_red(ref);
                    }
                });
            }
        }

        function tabchange_orange_update() {
            products_list = get_unique_products();
            $(add_inq_btn).each(function () {
                var data_post_id_all = $(this).attr('data-post-id');
                ref = this;
                if (products_list.length > 0) {
                    update_view_list_number();
                    if (products_list.includes(data_post_id_all)) {
                        on_click_orange(ref);
                    } else {
                        // on_click_red(ref);
                    }
                } else {
                    update_view_list_number();
                }
            });
        }

        function tabchange_red_update() {
            products_list = get_unique_products();
            $(remove_inq_btn).each(function () {
                var data_post_id_all = $(this).attr('data-post-id');
                ref = this;
                if (products_list.length > 0) {
                    update_view_list_number();
                    if (products_list.includes(data_post_id_all)) {
                        // on_click_red(ref);
                    } else {
                        on_click_red(ref);
                    }
                } else {
                    update_view_list_number();
                }
                // console.log('Updateding Red Buttons -> making them Orange');
            });
        }

        function ontabchange() {
            update_view_list_number();
            //if item is in store list but not in  product list or vice versa
            tabchange_orange_update();
            tabchange_red_update();
        }
        ontabchange();

        if (inquire_products.length != 0) {
            product_list = store.get('inquire_products');
            fr_show_toast();
            update_view_list_number();
            $(add_inq_btn).each(function () {
                ref = this;
                var all_post_ids = $(this).attr('data-post-id');
                if (product_list.indexOf(all_post_ids) > -1) {
                    simlar_products_check();
                }
            });
        } else {
            update_view_list_number();
        }

        $(add_inq_btn).one('click', function (e) {
            e.preventDefault();
            update_view_list_number();
        });

        // Add Item Button
        $(add_inq_btn).each(function () {
            that = add_inq_btn;
            $(document).on('click', that, function (e) {
                e.preventDefault();
                var data_post_id = $(this).attr('data-post-id');
                ref = this;
                post_id = data_post_id;
                add_item(ref, post_id);
            });
        });

        // Remove Item Button
        $(remove_inq_btn).each(function () {
            that = remove_inq_btn;
            $(document).on('click', that, function (e) {
                e.preventDefault();
                var data_post_id = $(this).attr('data-post-id');
                ref = this;
                post_id = data_post_id;
                remove_item(ref, post_id);
            });
        });

        // Update List status and Button after changing tabs
        $(window).on('focus', function () {
            console.log('Welcome Back :)');
            simlar_products_check();
            ontabchange();
        });
    } //end if

    //Inquire List Page
    if ($('body').hasClass('page-id-7896')) {
        var all_prod_count;
        if (typeof store.get('inquire_products') != 'undefined') {
            all_prod_count = store.get('inquire_products').length;
            if (all_prod_count == 0) {
                generate_table();
            }
        } else {
            all_prod_count = 0;
            generate_table();
        }
        var count = 0;

        function load_products_data() {
            window.products = {};
            $.each(store.get('inquire_products'), function (i, v) {
                var p_data = [];
                $.get('/wp-json/api/v1/post-id', {
                    id: v
                }, function (resp) {
                    p_data['title'] = resp.title;
                    p_data['link'] = resp.link;
                    p_data['img'] = resp.img;
                    p_data['sku'] = resp.sku;

                    if (p_data['title'] != undefined) {
                        count++;
                    }

                    if (all_prod_count == count) {
                        generate_table();
                    }
                });
                window.products[v] = p_data;
            });
        }

        load_products_data();

        function generate_table() {

            var output = '<table id="enquiry-table">';
            output += '<thead><tr><th>Product</th><th>Product SKU</th><th>View Product</th><th>Qty.</th><th>Remove</th></tr></thead><tbody>';

            $.each(window.products, function (i, v) {
                output += '<tr data-post-id="' + i + '">';
                output += '<td><a href="' + v.link + '"><img width="75" height="75" src="' + v.img + '">' + v.title + '</a></td><td>' + v.sku + '</td><td><a href="' + v.link + '" class="view-product-link" target="_blank">View</a></td><td><input type="number" value=1 class="quantity" placeholder="Qty"></td><td><a data-post-id="' + i + '" href="javascript:void(0)" class="remove-item">Remove</a></td>';
                output += '</tr>';
            });
            output += '</tbody></table>';
            $('.page-id-7896 #enquiry-list').html(output);

            //Set SKUs to hidden field START
            var p_skus = [];
            var p_ids = [];
            $.each(window.products, function (k, v) {
                p_skus.push(v.sku);
                p_ids.push(k);
            });
            $('#input_1_3').val(p_skus.toString());
            $('#input_1_7').val(p_ids.toString());
            
            //Set SKUs to hidden field END
            validateQty_onKeyup();

        }

        $(document).on('click', '.remove-item', function (e) {
            // console.log(this)
            e.preventDefault();
            var saved_products = store.get('inquire_products');
            var remove_city = $(this).attr('data-post-id');
            var index = $.inArray(remove_city, saved_products);
            if (index > -1) {
                saved_products.splice(index, 1);
            }
            store.set('inquire_products', saved_products);
            $(this).parent().parent().fadeOut();
        });

        function destroy_table(){
            $('#enquiry-table').remove();
            var empty_list = [];
            store.set('inquire_products', empty_list);
        }

        jQuery(document).on('gform_confirmation_loaded', function(){
            destroy_table();
        });

        function Valid(elem) {
            var elem = elem;
            var elem_parent = $(elem).parent();
            $('.error-tip', elem_parent).remove();
        }
        function inValid(elem) {
            var elem = elem;
            var tooltip_div = '<div class="error-tip">Enter valid Quantity</div>';
            $(elem).after(tooltip_div);
        }

        function validateQty_onKeyup(){
            $('#enquiry-table .quantity').on('keyup', function(){
                // console.log('Value Changed');
                var elem = this;
                var this_parent = $(this).parent();


                var qty_length = $('#enquiry-table .quantity').length;
                var valid_inp = 0;


                // Remove Error msg if it already presnt
                if($('.error-tip', this_parent).length > 0){
                    $('.error-tip', this_parent).remove();
                }
                // Regenrate Errors on keyup
                if($(this).val() != '' && $(this).val() > 0){
                    Valid(elem);
                    valid_inp++;
                    if(valid_inp==qty_length){
                        enableSubmit();
                        valid_inp = 0;
                    }
                }
                else{
                    inValid(elem);
                    disableSubmit();
                }

                var qty_length = $('#enquiry-table .quantity').length;
                var valid_inp = 0;
                $('#enquiry-table .quantity').each(function () {
                    if ($(this).val() != '' && $(this).val() > 0) {
                        valid_inp++;
                        if(valid_inp==qty_length){
                            enableSubmit();
                            valid_inp = 0;
                        }
                    }
                });

            });
        }
    
        function validateQty_onSubmit(){
            $('.error-tip').remove();
            $('#enquiry-table .quantity').each(function () {
                var elem = this;
                if ($(this).val() != '' && $(this).val() > 0) {
                    Valid(elem);
                } else {
                    inValid(elem);
                    disableSubmit();
                }
            });
        }

        function disableSubmit(){
            $('#gform_submit_button_1').css({'cursor':'not-allowed'}).attr('disabled', true);
        }
        function enableSubmit(){
            $('#gform_submit_button_1').css({'cursor':'pointer'}).attr('disabled', false);
        }

        $('.remove-item').on('click', function(){
            checkQty();
        });

        function checkQty(){
            validateQty_onSubmit();
            var qty_length = $('#enquiry-table .quantity').length;
            var valid_inp = 0;
            $('#enquiry-table .quantity').each(function () {
                if ($(this).val() != '' && $(this).val() > 0) {
                    valid_inp++;
                    if(valid_inp==qty_length){
                        enableSubmit();
                        valid_inp = 0;
                    }
                }
            });
        }
        // disableSubmit();
        checkQty();

        jQuery('#gform_1').submit(function(e){
            var prod_qty = [];
            var html_qty = jQuery('#enquiry-table .quantity');
            // var fill_qty_alert = false;
            jQuery.each(html_qty, function(k, v){
                var qty = jQuery(v).val();
                if( qty != '' ){
                    prod_qty.push(qty);
                }
            });
            // if( prod_qty.length < 4 ){
            //     fill_qty_alert = false;
            // }else{
            //     jQuery('#gform_ajax_spinner_1').show();
            //     fill_qty_alert = true;
            // }
            // if( fill_qty_alert == false ){
            //     jQuery('#gform_ajax_spinner_1').hide();
            //     alert('Please fill in quantity values for all the products.');
            //     e.preventDefault();
            //     return false;
            // }
            jQuery('#input_1_8').val( prod_qty.toString() );
        });

        //Reload table
        // $(window).on('focus', function () {
        //     // console.log('Welcome Back :)');
        //     if (typeof store.get('inquire_products') != 'undefined' && all_prod_count != store.get('inquire_products').length){
        //         load_products_data();
        //     }
        // });
    }
});