<?php
/**
 * The Header template for our theme
 *
 * @package WordPress
 * @subpackage Outstock_theme
 * @since Outstock Themes 1.1
 */
?>
<?php 

$outstock_opt = get_option( 'outstock_opt' );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	
	 <link rel="preload" href="/wp-content/themes/outstock/fonts/montserrat-medium-webfont.woff" as="font" type="font/woff" crossorigin>
	<link rel="preload" href="/wp-content/plugins/ubermenu/assets/fontawesome/webfonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin defer>
	
	<meta name="p:domain_verify" content="741f82e75c524514ab1ed8d3963d3741"/>
	<?php 

$cat_id = single_cat_title('', false);
if($cat_id == ''){// IF NO CATEGORY
}else{// IF CATEGORY FOUND
	$current_url  = set_url_scheme( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
	
	$request_url = $_SERVER['REQUEST_URI'];
	
	//$href_lang = array("de","de","es","es","pt","pt","fr","fr","nl","nl","it","it","ru","ru","en_in");
	$href_lang = array("de","es","pt","fr","nl","it","ru","en_in","australia");
	
	$request_url_parts = explode('/', $request_url);
 	if( in_array($request_url_parts[1],$href_lang)){
		$request_url = preg_replace('/^(\/(.*?)\/)/', '/', $request_url);
	}

	$segments = explode('/', trim(parse_url($request_url, PHP_URL_PATH), '/'));
	
	//echo count($segments);
	if(count($segments) == 2){
		$request_url = str_replace("/c","",$request_url);
	}
	$segments = explode('/', trim(parse_url($request_url, PHP_URL_PATH), '/'));
	//echo "<pre>";
	//print_r($segments);
    if($segments[0]=='de' || $segments[0]=='es' || $segments[0]=='pt' || $segments[0]=='fr' || $segments[0]=='nl' || $segments[0]=='it' || $segments[0]=='ru' || $segments[0]=='en_in' || $segments[0]=='australia'){
		$path = $segments[1].'/'.$segments[2].'/'.$segments[3];
	}
	else{
		$path = $segments[0].'/'.$segments[1].'/'.$segments[2];
	}
	


global $wpdb;

	
$post_id = $wpdb->get_results("SELECT * FROM fr_premmerce_filter_seo WHERE (path = '". $path ."')");
//print_r($post_id);
//echo "(path = '/". $path ."')";	
	
  
//print_r($post_id);
if(!empty($post_id) && $post_id[0]->noindex ==1){
	echo '<meta name="Googlebot" content="index, follow">';
	echo '<meta name="robots" content="index, follow">';
	//echo 'index';
	
    add_filter( 'rank_math/frontend/robots', function( $robots ) {
        unset( $robots['index'] );
        unset( $robots['follow'] );
        return $robots;
    });
}
else{
	//$count = $wpdb->get_results("SELECT COUNT(*) as available FROM fr_premmerce_filter_seo WHERE (path = '". $path ."')");
	//if($count[0]->available !=0){

		if(!empty($segments[1])){
		echo '<meta name="Googlebot" content="noindex, nofollow">';
		echo '<meta name="robots" content="noindex, nofollow">';
		//	echo 'noindex';
			
		
		add_filter( 'rank_math/frontend/robots', function( $robots ) {
			unset( $robots['index'] );
			unset( $robots['follow'] );
			return $robots;
		});
	}
	
	
}

}
?>
	
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M897TWF');</script>
	
	
	
	
<!-- End Google Tag Manager -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>

</head>
<?php
	$outstock_layout = (isset($outstock_opt['page_layout']) && $outstock_opt['page_layout'] == 'box') ? 'box-layout':'';
	$outstock_header = (empty($outstock_opt['header_layout']) || $outstock_opt['header_layout'] == 'default') ? 'first': $outstock_opt['header_layout'];
	if(get_post_meta( get_the_ID(), 'lionthemes_header_page', true )){
		$outstock_header = get_post_meta( get_the_ID(), 'lionthemes_header_page', true );
	}
	if(get_post_meta( get_the_ID(), 'lionthemes_layout_page', true ) && !is_search()){
		$outstock_layout = (get_post_meta( get_the_ID(), 'lionthemes_layout_page', true ) == 'box') ? 'box-layout' : '';
	}
?>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M897TWF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
<div class="main-wrapper <?php echo esc_attr($outstock_layout); ?>">
<?php do_action('before'); ?> 
	<header>
	<?php
		get_header($outstock_header);
	?>
	</header>
	<div id="content" class="site-content">
