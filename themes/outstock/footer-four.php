<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Outstock_Themes
 * @since Outstock Themes 1.1
 */
?>
<?php 
$outstock_opt = get_option( 'outstock_opt' );
?>
<style>
	#site-footer .footer-top .col-half-offset {
		margin: 0 1.58% !important;
	}
</style>
	<div class="footer layout4">
		<?php if ( !empty($outstock_opt['back_to_top'])) { ?>
			<div id="back-top" class="hidden-xs"><i class="fa fa-angle-double-up"></i></div>
		<?php } ?>

		<?php if (is_active_sidebar('footer_widget_column')) { ?> 
		<div class="footer-widget-column">
			<div class="container">
				<div class="row">
					<div class="widget-footer footer_widget_column">
						<?php dynamic_sidebar('footer_widget_column'); ?> 
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if (is_active_sidebar('footer_widget_social_column')) { ?> 
		<div class="footer-widget-social-column">
			<div class="container">
				<div class="row">
					<div class="widget-footer footer_widget_social_column">
						<?php dynamic_sidebar('footer_widget_social_column'); ?> 
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if (is_active_sidebar('footer_widget_newsletter')) { ?> 
		<div class="footer-newsletter">
			<div class="container">
				<div class="row">
					<div class="widget-footer footer_widget_newsletter">
						<?php dynamic_sidebar('footer_widget_newsletter'); ?> 
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if (is_active_sidebar('footer_4columns')) { ?> 
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('footer_4columns'); ?>
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="footer-location">
			<div class="conatiner">
			
			</div>
		</div>
		
		<div class="container">

			<?php /* OLD IMPLEMENTATION
			<!-- <div class="footer-bottom"> -->
				<!-- <div class="row"> -->
					<!-- <div class="col-sm-6">
						<div class="widget-copyright">
							<?php 
							if( !empty($outstock_opt['copyright']) ) {
								echo wp_kses($outstock_opt['copyright'], array(
									'a' => array(
										'href' => array(),
										'title' => array()
									),
									'br' => array(),
									'em' => array(),
									'strong' => array(),
								));
							} else {
								echo '&copy; '.date('Y').' <a href="'.esc_url( home_url( '/' ) ).'">'.get_bloginfo('name').'.com</a>';
							}
							?>
						</div>
					</div>
					<?php //if(!empty($outstock_opt['social_icons'])) { ?>
					<div class="col-sm-6">
						<?php
							// echo '<ul class="link-follow">';
							// foreach($outstock_opt['social_icons'] as $key=>$value ) {
							// 	if($value!=''){
							// 		if($key=='vimeo'){
							// 			echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
							// 		} else {
							// 			echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
							// 		}
							// 	}
							// }
							// echo '</ul>';
						?>
						<style>
							ul.footer-page-link img.huzz-logo{
								height:auto;
								width: 25px;
							}
							ul.footer-page-link li.huzz {
								padding-left: 0 !important;
							}
						</style>
						<ul class="footer-page-link">
							<li><a href="/frequently-asked-questions/">FAQs</a></li>
							<li><a href="/privacy-policy/">Privacy Policy</a></li>
							<li><a href="/terms-of-use/">Terms of Use</a></li>
							<li class="huzz"><a href="https://www.houzz.in/pro/webuser-760996599/desert-furnish-design-hub-pvt-ltd"><img src="https://st.hzcdn.com/static_en-IN/badge36_36@2x.png" alt="HU-760996599 in Jodhpur, Rajasthan, IN on Houzz" width="36" height="36" border="0" /></a></li>
						</ul>
					</div> -->
					OLD IMPLEMENTATION */ ?>

					<?php /* New Implementation */ ?>
					<div class="footer-bottom">
						<div class="row" style=" text-align: center;">
							<div class="col-lg-6 col-md-12 footer-new-imp" style="">
								<style>
									@media screen and (max-width:500px){
										.footer .footer-bottom{
											padding-bottom:60px !important;
											padding-top:0px !important;
										}
										.footer .footer-bottom .row{
											text-align: center;
										}
										footer .footer-bottom ul.footer-page-link{
											text-align:center !important;
											padding-left:0;
										}
									}
									ul.footer-page-link img.huzz-logo{
										height:auto;
										width: 25px;
									}
									ul.footer-page-link li.huzz {
										padding-left: 0 !important;
									}
									footer .footer-bottom ul.footer-page-link {
										list-style: none;
										text-align: left;
									}
								</style>
								<ul class="footer-page-link">
									<li class="huzz"><a href="https://www.houzz.in/pro/webuser-760996599/desert-furnish-design-hub-pvt-ltd" style="position: relative;top: -5px;"><img src="https://st.hzcdn.com/static_en-IN/badge36_36@2x.png" alt="HU-760996599 in Jodhpur, Rajasthan, IN on Houzz" width="36" height="36" border="0" style="martin-bottom: 59px;"></a></li>
									<li><a href="/frequently-asked-questions/">FAQs</a></li>
									<li><a href="/privacy-policy/">Privacy Policy</a></li>
									<li><a href="/terms-of-use/">Terms of Use</a></li>
								</ul>
							</div>
						</div>
					</div>
					<?php //} ?>
				<!-- </div> -->
			<!-- </div> -->
		</div>
		
	</div>