<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Outstock_Themes
 * @since Outstock Themes 1.1
 */
 
$outstock_opt = get_option( 'outstock_opt' );
$logo = ( !empty($outstock_opt['logo_main']['url']) ) ? $outstock_opt['logo_main']['url'] : '';
if(get_post_meta( get_the_ID(), 'outstock_logo_page', true )){
	$logo = get_post_meta( get_the_ID(), 'outstock_logo_page', true );
}
?>
<div class="top-1 row">
 <div class="top-this top1">We Deliver And Ship Our Furniture Across  <?php echo do_shortcode("[userip_location type=country]"); ?> <?php echo do_shortcode("[userip_location type='flag' height='15px' width='25px']"); ?>  </div>
</div> 
	<div class="header-container layout2">
		
			<div class="header">
				<div class="row">
					
					<div class="col-md-12 col-sm-12 col-xs-12 col-search">
						
					
						<div class="logo pull-left"><a href="/" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"/></a></div>
						
						
						<div class="nav-menus">
							<div class="nav-desktop visible-lg visible-md">
								<?php if( function_exists('ubermenu')) { ?>
									<?php ubermenu( 'furnitureroots' , array( 'menu' => 74 ) ); ?>
								<?php }else { ?>
									<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'primary-menu-container', 'menu_class' => 'nav-menu' ) ); ?>
								<?php } ?>
							</div>
							
							<div class="nav-mobile visible-xs visible-sm">
								<div class="mobile-menu-overlay"></div>
								<div class="toggle-menu"><i class="fa fa-bars"></i></div>
								<div class="mobile-navigation">
									<?php wp_nav_menu( array( 'theme_location' => 'mobilemenu', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu mobile-menu' ) ); ?>
								</div>
							</div>
						</div>
							<div class="action pull-right visible-xs">
							<?php if(is_active_sidebar('top_header')){ ?>
								<div class="header-top-setting pull-right">
									<i class="ion ion-navicon"> </i>
									<div class="setting-container">
									<?php if (is_active_sidebar('top_header')) { ?> 
										<?php dynamic_sidebar('top_header'); ?> 
									<?php } ?>
									</div>
								</div>
							<?php } ?>
							
								
							<?php if(class_exists('WC_Widget_Product_Search')) { ?>
							<div class="top-search pull-right">
								<div class="dropdown">
									<div class="dropdown-toggle">
										<div class="top-search">
											<a href="javascript:void(0)"><i class="ion ion-ios-search-strong"></i><span class="search-text"><?php echo esc_html__('Search', 'outstock') ?></span></a>
										</div>
									</div>
									<div class="search-container">
										<?php the_widget('WC_Widget_Product_Search', array('title' => '')); ?>
									</div>
								</div>
							</div>
							<?php } ?>
							</div>
					</div>
				</div>
			</div>
<!-- 		<div class="clearfix"></div> -->
	</div>