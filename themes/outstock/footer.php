<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Outstock_theme
 * @since Outstock Themes 1.1
 */
?>
<?php 
	$outstock_opt = get_option( 'outstock_opt' ); 
	$outstock_footer = (!isset($outstock_opt['footer_layout']) || $outstock_opt['footer_layout'] == 'default') ? 'first' : $outstock_opt['footer_layout'];
	if(get_post_meta( get_the_ID(), 'lionthemes_footer_page', true )){
		$outstock_footer = get_post_meta( get_the_ID(), 'lionthemes_footer_page', true );
	}
	$content_layout = '';
	if(get_the_ID()){
		$content_layout = get_post_meta( get_the_ID(), 'lionthemes_content_layout', true );
	}
?>
		
		</div><!--.site-content-->
		<footer id="site-footer" class="<?php echo ($content_layout) ? esc_attr('footer_' . $content_layout):''; ?>">
			<?php
				get_footer($outstock_footer);
			?>
			
	
			
		</footer>
<div class="footer-1">
  <a href="https://wa.me/+919769003311?text=Hello%20I%20want%20to%20inquire%20about%20some%20products%20from%20the%20website"><div class="foot-this the2"> Whatsapp </div></a>
  <a href="tel:+919769003311"><div class="foot-this the1">Call Us</div></a>
  <a href="#popmake-42138"><div class="foot-this the3">Mail Us</div></a>
</div> 


	</div><!--.main wrapper-->
	<?php wp_footer(); ?>
	<script>
	window.addEventListener('DOMContentLoaded', function() {
		(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)
	});
	</script>
 <script>
	

		
 jQuery(document).ready(function(){
	
      jQuery('#links a').attr('target', '_blank');
	  jQuery( ".widget_shopping_cart" ).remove();
	jQuery(".widget_shopping_cart").empty();
    });
	</script>
<script>
	jQuery(document).ready(function () {
        var data = [];
        $('link').each(function () {
            $hreflang = $(this).attr('hreflang');
            if ($hreflang) {
                if ($.inArray($hreflang, data) == -1) {
                    data.push($hreflang);
                } else {
                    $(this).remove()
                }
            }
        });
    })
	</script>

<script>jQuery.event.special.touchstart = {
  setup: function( _, ns, handle ){
    if ( ns.includes("noPreventDefault") ) {
      this.addEventListener("touchstart", handle, { passive: false });
    } else {
      this.addEventListener("touchstart", handle, { passive: true });
    }
  }
};
</script>



</body>
</html>