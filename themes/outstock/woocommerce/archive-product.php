<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.5
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>
<?php

global $outstock_opt, $wp_query, $woocommerce_loop, $outstock_viewmode;
$term = get_queried_object();
$shopsidebar = 'left';
if(!isset($outstock_opt['sidebarshop_pos'])) {
	$shopsidebar = $outstock_opt['sidebarshop_pos']	;
}
if(isset($_GET['side'])) {
	$shopsidebar = $_GET['side'];
}

$shopcol = 12;
$productcols = 4;
$woocommerce_loop['shop_fullwidth'] = true;
if ($shopsidebar && is_active_sidebar( 'shop' )) {
	$shopcol = 9;
	$productcols = 3;
	$woocommerce_loop['shop_fullwidth'] = false;
}

$outstock_viewmode = 'grid-view';
if(isset($outstock_opt['default_view'])) {
	if($outstock_opt['default_view']=='list-view'){
		$outstock_viewmode = 'list-view';
	}
}
if(isset($_GET['view']) && $_GET['view']=='list-view'){
	$outstock_viewmode = $_GET['view'];
}

$shop_view = get_option('woocommerce_shop_page_display');
$cat_view = get_option('woocommerce_category_archive_display');
$detect_pro_view = true;
$cateID = 0;
$showsubcats = false;
if (is_product_category()) {
	$detect_pro_view = ($cat_view != 'subcategories');
	$cate = get_queried_object();
	$cateID = $cate->term_id;
	$display_type = get_term_meta($cateID, 'display_type'); 
	if(!empty($display_type[0]) && ($display_type[0] == 'products' || $display_type[0] == 'both')) $detect_pro_view = true;
	if(!empty($display_type[0]) && $display_type[0] == 'subcategories') $detect_pro_view = false;
	if(!empty($display_type[0]) && ($display_type[0] == 'subcategories' || $display_type[0] == 'both')) $showsubcats = true;
}
if(is_shop() && $shop_view == 'subcategories'){
	$detect_pro_view = false;
}
if(is_search() || count(WC_Query::get_layered_nav_chosen_attributes()) > 0) $detect_pro_view = true;
if (is_shop() && $shop_view != 'products' && $shop_view) $showsubcats = true;
?>
<div class="main-container">
	<div class="page-content">
		<div class="archive-banner" style="background:#F7F7F7; padding-top:40px; position: relative;text-align: center;">
			<h1 class="vc_custom_heading vc_custom_1597929061877" style="font-size: 24px;color: #333333;text-align: center; margin:0px;">
						<?php
						 if ( is_category() ) {
							$title = single_cat_title( '', false );
						} elseif ( is_tag() ) {
							$title = single_tag_title( '', false );
						} elseif ( is_author() ) {
							$title = '<span class="vcard">' . get_the_author() . '</span>';
						} elseif ( is_post_type_archive() ) {
							$title = post_type_archive_title( '', false );
						} elseif ( is_tax() ) {
							$title = single_term_title( '', false );
						}
					   

						if(!empty(do_action( 'woocommerce_page_titleH1' ))) {
						
							do_action( 'woocommerce_page_titleH1' );
							//$title = single_term_title( '', false );
							
						}else { 
							 echo ' '.$title; 
						}
							?>
						</h1>	
</div>
		
		
		
		<div class="archive-banner" style="background:#F7F7F7; padding-top:0px; position: relative;text-align: center;">
			<?php 
					//single_cat_title('')
						// For Product Category pages
						// If Attribute filters are not requested then only display below breadcrumbs else hide.
						if (empty($_GET)) {
					?>
						<?php if( have_rows('breadcrumbsfr', $term) ): ?>
								
						   <?php 
						   		while( have_rows('breadcrumbsfr', $term) ) : the_row();
       					 		$category = get_sub_field('name', $term);
						  		$url = get_sub_field('url', $term); 
							?>					 
    						 	<a href="<?php echo $url; ?>" target="_blank" style="font-family: Montserrat,sans-serif; color: #323232; font-size: 12px; content-visibility:auto;">								
									<?php echo $category ?> /
    							</a>
							<?php
								endwhile;
							?>
								
						<?php endif; ?>
					<?php 
						}
					?>
			
	
			
</div>
		
		
		
		<div class="archive-banner" style="background:#F7F7F7; padding-top:0px; position: relative;text-align: center;"><div class="container-fluid category-desc raw1">
							<?php do_action( 'woocommerce_archive_description' ); ?>
<?php echo do_shortcode( '[contact-form-7 id="36373" title="Untitled"]' ); ?><p class="consulta" style="content-visibility:auto; margin-bottom:0px;">Speak to our furniture consultant for more details on pricing and designs!</p><p  class="consulta" style="content-visibility:auto;">Also, let us know a suitable time to connect with you via mail/call </p></div></div>
		
		
		
		<div class="archive-banner" style="background:#fff; padding-top:40px; position: relative;text-align: center;">						

			<div class="container container-bowl">
					<div class="row rep-field-row">
						<?php if( have_rows('related_categories', $term) ):
   						 while( have_rows('related_categories', $term) ) : the_row();
       					 $category = get_sub_field('related_category', $term);
       					 $image = get_sub_field('image', $term);
      					 $description = get_sub_field('description', $term);
     					 $url = get_sub_field('url', $term); ?>
   							 <div class="container rep-field">
    						 	
									<div class="row-new-rep">
										
 										<div class="col-rep-s">
											<img src="<?php echo $image; ?>" width="100px" height="100px">
										</div>
  										<div class="col-rep-l">
											<a href="<?php echo $url; ?>" target="_blank">
											<p class="h2title"><?php echo $category ?></p>
												</a>
    										<p><?php echo $description ?></p>
										</div>
    		
									</div>
    							
    						</div>  
						

						<?php
   					    endwhile;
						endif;
						?>
					</div>
				
				
			</div>
			
			
		
		<div class="container">
					<div class="row rep-field-row">
						
						<?php if( have_rows('customize_banner', $term) ):
   						 while( have_rows('customize_banner', $term) ) : the_row();
       					 
       					 $image = get_sub_field('banner', $term);
      					 
     					 $url = get_sub_field('url', $term); ?>
   							 <div class="container rep-field-banner">
								 <br/>
    						 	<a href="<?php echo $url; ?>" target="_blank"><img src="<?php echo $image; ?>"></a>
    						</div>  
						

						<?php
   					    endwhile;
						endif;
						?>
					</div>
				
				
			</div>	
				
			
		</div>
		
		
		
		
		
		<div class="container shop_content">
				
				<div class="row">
					<?php if( $shopsidebar == 'left' ) :?>
						<?php if(isset($_GET['test'])) 
						{ 
							echo 'yes';
							get_sidebar('shop');
							} ?>
						<?php get_sidebar('shop'); ?>
					<?php endif; ?>
					<div id="archive-product" class="col-xs-12 <?php echo 'col-md-'.$shopcol; ?>">
						<script type="application/ld+json">
						{
						  "@context": "http://schema.org",
						  "@type": "ItemList",
						  "itemListElement": [
						  <?php  
						  	global $product;
						  	$rows = 0; 
						  	$count = 0;
						  	while( have_posts() ) : the_post(); 
						  		$rows++; 
						  	endwhile; 

						  	while( have_posts() ) : the_post(); 
						  		$img = wp_get_attachment_url( $product->get_image_id() ); 
						  		$url = get_permalink( $product->ID );  
						  		$name = esc_attr( $product->get_name());  
						  		$price = $product->get_sale_price();;
						  		$count++;
						  	?> 
						  		{
						  			
						   			"@type": "ListItem",
						   			"position": <?php echo $count; ?>,
						   			"url": "<?php echo $url; ?>",
						   			"name": "<?php echo $name; ?>"
			  					}<?php if($rows !== $count) echo ','; 
			  				endwhile; ?>
						  	]
						}
						</script>
						
						

						<div class="archive-border">
							<?php if ( have_posts() ) : ?>
								
								<?php
									/**
									* remove message from 'woocommerce_before_shop_loop' and show here
									*/
									do_action( 'woocommerce_show_message' );
								?>
								<?php if($showsubcats){ ?>
								<?php /*
									<div class="shop-categories categories row">
										<?php woocommerce_output_product_categories(array('parent_id' => $cateID));
										//reset loop
										$woocommerce_loop['loop'] = 0; ?>
									</div>
								*/ ?>
								<?php } ?>
								<?php if($detect_pro_view){ ?>
								<div class="toolbar">
									
									<?php
										/**
										 * woocommerce_before_shop_loop hook
										 *
										 * @hooked woocommerce_result_count - 20
										 * @hooked woocommerce_catalog_ordering - 30
										 */
										do_action( 'woocommerce_before_shop_loop' );
									?>
									<div class="clearfix"></div>
								</div>
							
							
								<?php //woocommerce_product_loop_start(); ?>
								<div class="shop-products products row <?php echo esc_attr($outstock_viewmode);?> <?php echo esc_attr($shoplayout);?>">
									
									<?php $woocommerce_loop['columns'] = $productcols; ?>
									
									<?php while ( have_posts() ) : the_post(); ?>

										<?php wc_get_template_part( 'content', 'product-archive' ); ?>

									<?php endwhile; // end of the loop. ?>
								</div>
								<?php //woocommerce_product_loop_end(); ?>
								
								<div class="toolbar tb-bottom<?php echo (!empty($outstock_opt['enable_loadmore'])) ? ' hide':''; ?>">
									<?php
										/**
										 * woocommerce_before_shop_loop hook
										 *
										 * @hooked woocommerce_result_count - 20
										 * @hooked woocommerce_catalog_ordering - 30
										 */
										do_action( 'woocommerce_after_shop_loop' );
										//do_action( 'woocommerce_before_shop_loop' );
									?>
									<div class="clearfix"></div>
								</div>
								<?php if(!empty($outstock_opt['enable_loadmore'])){ ?>
									<div class="load-more-product text-center">
										<?php if($outstock_opt['enable_loadmore'] == 'button-more'){ ?>
											<img class="hide" src="<?php echo get_template_directory_uri() ?>/images/small-loading.gif" alt="" />
											<a class="button" href="javascript:loadmoreProducts()"><?php echo esc_html__('Load more', 'outstock'); ?></a>
										<?php }else{ ?>
											<img width="100" class="hide" src="<?php echo get_template_directory_uri() ?>/images/big-loading.gif" alt="" />
										<?php } ?>
									</div>
								<?php } ?>
								<?php } ?>
							<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

								<?php wc_get_template( 'loop/no-products-found.php' ); ?>

							<?php endif; ?>
						</div>
					</div>
					<?php if($shopsidebar == 'right') :?>
						<?php get_sidebar('shop'); ?>
					<?php endif; ?>
				</div>
			
			<h3 class="text-center">Related Categories</h3>
			<div class="container container-bowl">
					<div class="row rep-field-row">
						<?php if( have_rows('related_categories_bottom', $term) ):
   						 while( have_rows('related_categories_bottom', $term) ) : the_row();
       					 $category = get_sub_field('related_category_bottom', $term);
       					 $image = get_sub_field('image_bottom', $term);
      					 $description = get_sub_field('description_bottom', $term);
     					 $url = get_sub_field('url_bottom', $term); ?>
   							 <div class="container rep-field">
    						 	
									<div class="row-new-rep">
										
 										<div class="col-rep-s">
											<img src="<?php echo $image; ?>" width="100px" height="100px">
										</div>
  										<div class="col-rep-l">
											<a href="<?php echo $url; ?>" target="_blank">
											<p class="h2title"><?php echo $category ?></p>
												</a>
    										<p><?php echo $description ?></p>
										</div>
									</div>
    						</div>  
						<?php
   					    endwhile;
						endif;
						?>
					</div>				
			</div>
			
				
			

				<?php 
				//  Category Bottom Content START
					// if(isset($_GET['frtest'])){
						$term = get_queried_object(); // get the current taxonomy term
						$bottom_content = get_field( 'kp_category_content', $term );
						if( !empty($bottom_content) ){
							echo '<section id="cat-bottom-content" style="padding: 50px 0;">';
							echo $bottom_content;
							echo '</section>';
						}
					// }
				//  Category Bottom Content END
				?>

		</div>
		
	
	</div>
</div>

<?php 
$current_id = get_queried_object()->term_id;
if(isset($_GET['test']) || $current_id == 284) {?>
<!-- Tack Our expert now START -->
<section class="cat-talk-to-expert">
	<?php //if(isset($_GET['test'])){?>
		<?php echo do_shortcode('[insert page="9651" display="content"]'); ?>
	<?php //} ?>
</section>
<!-- Tack Our expert now END -->

<!-- FAQ Section START -->
<section id="faq-section" style="padding: 50px 0;">
	<div class="container">
		<div class="row">
			<h3 class="text-center" style="text-transform: uppercase; font-size: 22px;">Frequently asked question about <?php echo single_term_title(); ?></h3>
			<div class="question-list">
				<?php $term = get_queried_object(); ?>
				<?php if( have_rows('faq_about_category', $term) ): ?>

					<ul class="questions-answers-list">

					<?php while( have_rows('faq_about_category', $term) ): the_row(); 

						$fr_question = get_sub_field('category_question', $term);
						$fr_answer = get_sub_field('category_answer', $term);

					?>
						<li class="question-answer-item">
							<span style="float: left; font-size: 18px; margin-right: 10px;"><strong>Q.</strong></span>
							<h4><?php echo $fr_question; ?></h4>
							<div class="faq-content" style="width:100%">
							<?php echo $fr_answer; ?>
							</div>
						</li>

					<?php endwhile; ?>

					</ul>

				<?php endif; ?>

				<?php
				if( have_rows('faq_about_category', $term ) ){
					$faqs_json = '<script type="application/ld+json">
					{
						"@context": "https://schema.org",
						"@type": "FAQPage",
						"mainEntity": [';
					while( have_rows('faq_about_category', $term ) ){
						the_row();
						$category_question = get_sub_field('category_question');
						$category_answer = get_sub_field('category_answer');

						$faqs_json .= '{"@type": "Question",
										"name": "'.strip_tags($category_question).'",
										"acceptedAnswer": {
											"@type": "Answer",
											"text": "'.esc_html($category_answer).'"
										}
									},';
					}
					$faqs_json = rtrim($faqs_json, ',');
					$faqs_json .= ']
					}
					</script>';
					echo $faqs_json;
				}
					
				?>
			</div>
		</div>
	</div>
</section>
<!-- FAQ Section END -->

<!-- Interested Section START -->
<section id="interested-section" style="background: #FBFBFB; padding: 50px 0;">
	<?php //if(isset($_GET['test'])){?>
	<div class="container">
		<div class="row">
			<h3 class="text-center">interested in <?php echo single_term_title(); ?> ?</h3>
			<p class="text-center">Submit your details and our expert will call you.</p>
			<div class="interested-form">
				<?php echo do_shortcode('[gravityform id=4 title=false]'); ?>
			</div>
		</div>
	</div>
	<?php //} ?>
</section>
<!-- Interested Section END -->

<section class="related-category">
	<div class="container">
		<div class="row">
			<?php if(isset($_GET['test'])){ ?>
				
				<h3 class="text-center">Explore Related Categories</h3>
				<?php
				$termParent = ($term->parent == 0) ? $term : get_term($term->parent, 'product_cat');
				$sibling_cats = wp_list_categories( array(
					'echo'                => 0,
					'taxonomy' => 'product_cat',
					'child_of'            => $termParent->term_id,
					'current_category'    => $term,
					'title_li'            => __( '' )
				) );
			} ?>
			<div class="cat-hierarchy">
				<div class="parent-cat">
					<h4><?php echo $termParent->name; ?></h4>
				</div>
					<ul class="sub-cats">
						<?php echo $sibling_cats; ?>
					</ul>
			</div>
		</div>
	</div>
</section>

<section class="blog-section">
	<div class="container">
		<div class="row">
		<h3 class="text-center">More Information from furnitureroots inhouse experts</h3>

		<?php 
		$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>2, 'order'=> 'ASC')); ?>
		
		<?php if ( $wpb_all_query->have_posts() ) : ?>
		<ul class="recent-blogs">
				<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?><br /><span><?php the_title(); ?></span></a></li>
				<?php endwhile; ?>
		</ul>
				<?php wp_reset_postdata(); ?>
		<?php endif; ?>

		</div>
	</div>
</section>

<?php } ?>

<?php get_footer( 'shop' ); 