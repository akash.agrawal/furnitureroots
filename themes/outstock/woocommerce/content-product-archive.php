<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.5
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop, $outstock_opt, $outstock_viewmode;
$time_modifiy = get_the_modified_date('Y-m-d');
$new_hot = '';
if(!empty($outstock_opt['new_pro_from'])){
	if(strtotime($time_modifiy) >= strtotime($outstock_opt['new_pro_from']) && !empty($outstock_opt['new_pro_label'])){
		$new_hot = '<span class="newlabel"><span>'. esc_html($outstock_opt['new_pro_label']) .'</span></span>';
	}elseif($product->is_featured() && !empty($outstock_opt['featured_pro_label'])){
		$new_hot = '<span class="hotlabel"><span>'. esc_html($outstock_opt['featured_pro_label']) .'</span></span>';
	}
}elseif($product->is_featured() && !empty($outstock_opt['featured_pro_label'])){
	$new_hot = '<span class="hotlabel"><span>'. esc_html($outstock_opt['featured_pro_label']) .'</span></span>';
}
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Extra post classes
$classes = array();
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == ( $woocommerce_loop['loop'] + 1 ) % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}

$count   = $product->get_rating_count();
$classes[] = 'item-col col-xs-12';
if($woocommerce_loop['shop_fullwidth']) {
	if(!empty($outstock_opt['product_per_row_fw'])){
		$woocommerce_loop['columns'] = $outstock_opt['product_per_row_fw'];
		$colwidth = round(12/$woocommerce_loop['columns']);
		$classes[] = 'col-sm-4 col-md-'.$colwidth;
	}
} else {
	if(!empty($outstock_opt['product_per_row'])){
		$woocommerce_loop['columns'] = $outstock_opt['product_per_row'];
		$colwidth = round(12/$woocommerce_loop['columns']);
		$classes[] = 'col-sm-'.$colwidth ;
	}
}
?>

<div <?php post_class( $classes ); ?> data-post-id=<?php echo get_the_ID(); ?>>
	<div class="product-wrapper">
		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		
		<div class="list-col4 <?php if($outstock_viewmode=='list-view'){ echo ' col-xs-12 col-sm-4';} ?>">
		
			<div class="product-image">
			
				<div class="product-label">
					<?php if ( $product->is_on_sale() ) : ?>
						<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale"><span class="sale-bg"></span><span class="sale-text">' . esc_html__( 'Sale', 'outstock' ) . '</span></span>', $post, $product ); ?>
					<?php endif; ?>
					<?php echo '' . $new_hot; ?>
				</div>
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( $product->get_name() ); ?>" target="_blank">
					<?php 
					echo wp_kses($product->get_image('shop_catalog', array('class'=>'primary_image')), array(
						'a'=>array(
							'href'=>array(),
							'title'=>array(),
							'class'=>array(),
						),
						'img'=>array(
							'src'=>array(),
							'height'=>array(),
							'width'=>array(),
							'class'=>array(),
							'alt'=>array(),
						)
					));
					
					if(isset($outstock_opt['second_image'])){
						if($outstock_opt['second_image']){
							$attachment_ids = $product->get_gallery_image_ids();
							if ( $attachment_ids ) {
								echo wp_get_attachment_image( $attachment_ids[0], apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ), false, array('class'=>'secondary_image') );
							}
						}
					}
					?>
				</a>
				
				<div class="actions">
					<ul class="add-to-links clearfix">
						
						
						<li class="add-to-inquery">
						  <a href="" class="add-inquiry-btn" data-post-id="<?php echo get_the_ID(); ?>">Get Quotes</a>
						
							<a class="adding">Adding...</a>
						  <a href="" class="remove-inquiry-btn" data-post-id="<?php echo get_the_ID(); ?>">Remove item</a>
						</li>
						
					</ul>
				</div>				
			</div>
		</div>
		<div class="list-col8 <?php if($outstock_viewmode=='list-view'){ echo ' col-xs-12 col-sm-8';} ?>">
			<div class="gridview">
				<div class="grid-info">
					<span class="product-name name-product">
						<a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
					</span>
					<div class="ratings"><?php echo wc_get_rating_html($product->get_average_rating()); ?></div>
					<div class="price-box"><?php echo ''.$product->get_price_html(); ?></div>
				</div>
							
			</div>
			
		</div>
		<div class="clearfix"></div>
		<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	</div>
</div>