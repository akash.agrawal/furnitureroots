Mar/2018: Released version 1.0
	-	You need read instruction in documentation folder or visit this url http://lion-themes.net/documentation/outstock


Updated:
1.1
- Fix: PHP error in author view and columns layout
- New home page 5, 6 for more demo.

1.0.1
- Fix: sub-categories not show correct by woocommerce setting
- Merge Shop layout & sidebar setting to one.
- Remove not in use theme options.