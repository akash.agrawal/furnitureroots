<?php

function add_theme_scripts() {
    wp_enqueue_script( 'store-js', get_template_directory_uri() . '/js/store.everything.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'inquiry-js', get_template_directory_uri() . '/js/inquiry-product.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), false, true );
    if(is_page(10489)){
    wp_enqueue_script( 'lazyload-js', get_template_directory_uri() . '/js/lazyload.min.js', array(), false, true );
    wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/js/lazysizes.min.js', array( 'jquery' ), false, true );
    }
    // wp_enqueue_script( 'unveilhooks', get_template_directory_uri() . '/js/ls.unveilhooks.min.js', array( 'lazysizes' ), false, true );
    wp_enqueue_style('slick-css', get_template_directory_uri() . '/css/slick.min.css', array(), '0.1.0', 'all');
    wp_enqueue_style('slick-theme-css', get_template_directory_uri() . '/css/slick-theme.css', array(), '0.1.0', 'all');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

add_filter( 'widget_text', 'do_shortcode' );

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

// Single product page Inquiry button
add_action( 'woocommerce_simple_add_to_cart', 'fr_add_inquiry_button', 50 );

function fr_add_inquiry_button() {
    // echo '<a href="" class="add-inquiry-btn" data-post-id="'.get_the_ID().'">Add to enquiry</a>';
    echo '<a href="" class="add-inquiry-btn" data-post-id="'.get_the_ID().'">Get Quotes</a>';
    echo '<a class="adding">Adding...</a>';
    echo '<a href="" class="remove-inquiry-btn" data-post-id="'.get_the_ID().'">Remove item</a>';
}

// Get post id for enquiry
add_action('wp_head', 'add_post_id');
function add_post_id() {
    if ( is_singular('product') ) {
        echo '<script>var post_id = '. get_the_ID() .'</script>';
    }
}

// Get product Data API START
function fr_product_details( $data ) {
    $product = wc_get_product($data['id']);
    $product_data = array(
        'title' => get_the_title( $data['id'] ),
        'link' => get_the_permalink( $data['id'] ),
        'img' => get_the_post_thumbnail_url( $data['id'] ),
        'sku' => $product->get_sku()
    );
    return $product_data;
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'api/v1', '/post-id', array(
        'methods' => 'GET',
        'callback' => 'fr_product_details'
    ) );
} );

add_action( 'wp_head', function(){ 
    ?>
    <!-- move inline css here -->
    <?php
    /*
    Queue inline jQuery code
    Source : http://writing.colin-gourlay.com/safely-using-ready-before-including-jquery/
    */
    ?>
    <script>(function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>
    <?php
}, 2);

add_action( 'wp_head', function(){ ?>


	<style type="text/css">
		
        </style>
<?php }, 11);

add_filter('wpseo_robots', 'set_pagination_noindex' );

function set_pagination_noindex( $value ){
	if( is_tax('product_cat') && is_paged() ){
		// if( is_ab_ppc_page() ) {
			return 'noindex,follow';
		// }
	}
	return $value;
}


//
add_action( 'gform_after_submission_1', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {
    $product_id_array = rgar( $entry, '7' );
    $buyer_name = rgar( $entry, '1' );
    $buyer_phone = rgar( $entry, '2' );
    $buyer_email = rgar( $entry, '4' );
    $contact_pref = rgar( $entry, '5' );
    $notes = rgar( $entry, '6' );
    $quantities = rgar( $entry, '8' );
    $quantities_arr = explode(',', $quantities);
    $p_id = explode(',', $product_id_array); // break string in to array

    $args = array(
        'include' => $p_id,
        'limit' => -1
    );

    $html .= '<table style="color: #333; padding:10px; border-collapse: collapse; border-spacing: 0;">';
    $html .= '<tr><th style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">Buyer Name</th>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">'.$buyer_name.'</td></tr>';
    $html .= '<tr><th style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">Buyer Phone</th>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">'.$buyer_phone.'</td></tr>';
    $html .= '<tr><th style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">Buyer Email</th>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">'.$buyer_email.'</td></tr>';
    $html .= '<tr><th style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">Contact Preference</th>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">'.$contact_pref.'</td></tr>';
    $html .= '<tr><th style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">Add. Details</th>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #fff; text-align: center; padding:10px;">'.$notes.'</td></tr>';
    $html .= '</table>';
    $html .= '<br/><br/>';
    
    $products = wc_get_products($args);
    $html .= '<table style="color: #333; padding:10px; border-collapse: collapse; border-spacing: 0;">';
    $html .= '<tr>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Sr. No.</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Product Image</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Product Name</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">SKU Code</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Qty. Reqd.</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">CBM (Unit)</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">CBM (Total)</th>';
    // $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Dimensions</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Price (Unit)</th>';
    $html .= '<th style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Price (Total)</th>';
    $html .= '</tr>';

    $cnt = 0;
    $total_cbm = 0;

    foreach ($products as $product) {
        $product_img = get_the_post_thumbnail_url($product->get_id());
        $name = $product->get_name();
        $dwps_specification_table = get_post_meta($product->get_id(), '_dwps_specification_table', true);
        $cbm_specs = $dwps_specification_table[3]['attributes'][1]['value'];
        $dimension = $dwps_specification_table[3]['attributes'][0]['value'];
        if( !isset( $cbm_specs ) ){
            $cbm_specs = 'Not set';
        }
        if( !isset( $dimension ) ){
            $dimension = 'Not set';
        }
        $cbm_add = $cbm_specs * $quantities_arr[$cnt];
        $total_cbm = $total_cbm + $cbm_add;

        $cbm_specs = number_format((float)$cbm_specs, 2, '.', '');
        $cbm_add = number_format((float)$cbm_add, 2, '.', '');

        $html .= '<tr>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"> '.($cnt + 1).' </td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"> <img src=" '.$product_img.'" style="width:60px; height:75px;" /> </td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"> ' . $name . '</td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">  ' . $product->get_sku() . '</td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"> '.$quantities_arr[$cnt].' </td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">  ' . $cbm_specs. '</td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">  ' . $cbm_add. '</td>';
        // $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">  ' . $dimension. '</td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">   </td>';
        $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"> </td>';
        $html .= '</tr>';

        $cnt++;
    }
    $total_cbm = number_format((float)$total_cbm, 2, '.', '');
    $html .= '<tr>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Total CBM = '.$total_cbm.'</td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;"></td>';
    $html .= '<td style=" border: 1px solid #CCC; height: 30px; background: #FAFAFA; text-align: center; padding:10px;">Total Price = </td>';
    $html .= '</td>';
    // $file = '/home/furnitureroots/public_html/wp-content/themes/outstock/data.txt';
    // file_put_contents($file, print_r($cbm_specs,true), FILE_APPEND);
    // $html .= '</html>';
    // $headers = "From FR rn";
    // $to_email = 'dhritiman@furnitureroots.com,business@furnitureroots.com,lavjeet@furnitureroots.com,kevin@furnitureroots.com';
    $to_email = 'dhritiman@furnitureroots.com,business@furnitureroots.com,kevin@furnitureroots.com';
	$subject = "New Enquiry Submission - ".$buyer_email."!";;
    $headers[] = 'MIME-Version: 1.0' . "rn";
    $headers[] = 'Content-Type: text/html; charset=ISO-8859-1' . "rn";
    // $headers[] = 'Bcc: abhishek.pawar@kraftpixel.com';
    // $headers[] = 'Bcc: nikhil.ratate@kraftpixel.com';
    // $headers[] = 'Bcc: milind.sonawane@kraftpixel.com';
    //file_put_contents($file, $person, FILE_APPEND);
    wp_mail( $to_email, $subject, $html, $headers );
}
//Added css fro full width ( product categories acf field)
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
  .taxonomy-product_cat #edittag  { max-width:100% !important; }
  </style>';
}


// remove WP 4.9+ dns-prefetch nonsense
remove_action( 'wp_head', 'wp_resource_hints', 2 );

/**
 * Override query for pages that match a category slug.
 * 
 * @param   WP  $wp
 */ 
function wpse_177014_category_to_page ( $wp ) {
    // if( isset( $_GET['nik'] ) ){
    //     echo '<pre>';
    //     var_dump($wp);
    //     die();
    // }
    $cat_arr_whitelist = array( 'hotel-furniture-resort-furniture-hospitality-furniture', 'hotel-resort-bedroom-furniture', 'restaurant-furniture-cafe-furniture-pub-furniture','cafe-furniture');
    
    if ( ! empty( $wp->query_vars['product_cat'] ) && in_array( $wp->query_vars['product_cat'], $cat_arr_whitelist ) && get_page_by_path( $slug = $wp->query_vars['product_cat'] ) ) {
        if ( ! empty( $wp->query_vars['paged'] ) )
            $page = $wp->query_vars['paged'];
        else
            $page = '';

        $wp->matched_query = "pagename=$slug&page=$page";
        $wp->query_vars    = array(
            'pagename' => $slug,
            'page'     => $page,    
        );
    }
}

add_action( 'parse_request', 'wpse_177014_category_to_page', 15 );

// add_filter('request', function(array $query_vars) {
//     // do nothing in wp-admin
//     if(is_admin()) {
//         return $query_vars;
//     }
//     // if the query is for a category
//     if(isset($query_vars['hotel-furniture-resort-furniture-hospitality-furniture'])) {
//         // save the slug
//         $pagename = $query_vars['hotel-furniture-resort-furniture-hospitality-furniture'];
//         // completely replace the query with a page query
//         $query_vars = array('pagename' => "$pagename");
//     }
//     return $query_vars;
// }, 15);

// function loadPageFirst() {
//     // get the actual category
//     $actualCategory = get_category( get_query_var('hotel-furniture-resort-furniture-hospitality-furniture') );
//     // get the page with the same slug
//     $matchingPage = get_page_by_path( $actualCategory->slug );

//     // If no match, load the normal listing template and exit (edit if you are using a custom listing template, eg. category.php)
//     if (!$matchingPage) {
//         include( get_template_directory() . '/archive.php');
//         die();
//     }

//     // Make a new query with the page's ID and load the page template
//     query_posts( 'page_id=' . $matchingPage->ID );
//     include( get_template_directory() . '/page.php');
//     die();
// }
// add_filter( 'category_template', 'loadPageFirst' );

//Add schema in pub table and chair category page start
// https://furnitureroots.com/pub-dining-set/
add_action('woocommerce_structured_data_breadcrumblist', 'kp_customschema_for_product_cat');
function kp_customschema_for_product_cat($schema){
    // if(isset($_GET['mishu'])){
        // var_dump($schema);
        if(!isset($schema['@type'])){
            return $schema;
        }

        if(isset($schema['@type']) && $schema['@type'] != 'BreadcrumbList'){
            return $schema;
        }

        if( is_tax('product_cat') && get_queried_object_id() == 775 ){
            // var_dump('<pre>');
            $schema = array();
            echo '<script type="application/ld+json">
            {
                "@context": "https://schema.org/", 
                "@type": "BreadcrumbList", 
                "itemListElement": [{
                    "@type": "ListItem", 
                    "position": 1, 
                    "name": "Home",
                    "item": "https://furnitureroots.com/"  
                },{
                    "@type": "ListItem", 
                    "position": 2, 
                    "name": "Pub Furniture",
                    "item": "https://furnitureroots.com/commercial-bar-pub-furniture/"  
                },{
                    "@type": "ListItem", 
                    "position": 3, 
                    "name": "Pub Table & Chair set",
                    "item": "https://furnitureroots.com/pub-dining-set/"  
                }]
            }
            </script>';
            return;
        }
    // }
}
//Add schema in pub table and chair category page end

// Add Schema on Following Category Pages - Task ID - FUR-T111
/*
https://furnitureroots.com/hospitality-reception-area-furniture/
https://furnitureroots.com/restaurant-bar-seating/
https://furnitureroots.com/restaurant-and-bar-tables-and-counters/
https://furnitureroots.com/hospitality-lamps-lighting/
https://furnitureroots.com/hotel-beds-bed-frames-and-bases/
https://furnitureroots.com/resort-and-hotel-bedsides-and-nightstands/
https://furnitureroots.com/resort-and-hotel-wardrobes/
https://furnitureroots.com/restaurant-chairs/
https://furnitureroots.com/restaurant-dining-sets/
https://furnitureroots.com/commercial-hanging-lights/
https://furnitureroots.com/cafe-chairs/
https://furnitureroots.com/cafe-tables/
https://furnitureroots.com/cafe-dining-set/
https://furnitureroots.com/pub-chairs-stools/
*/
add_action('kp_genrate_breadcrumb_schema_custom_action', 'kp_customschema_for_product_cat_new', 10 , 1);
function kp_customschema_for_product_cat_new( $breadcrumb ){
    $kp_schema_cat_ids = array( 396,95,96,384,313,317,315,285,296,387,770,769,771,774,290,97);
    $id = get_queried_object_id();
    if( is_tax('product_cat') && in_array( $id, $kp_schema_cat_ids ) ){
        $markup                    = array();
        $markup['@context']           = 'https://schema.org/';
        $markup['@type']           = 'BreadcrumbList';
        $markup['itemListElement'] = array();
        foreach ( $breadcrumb as $key => $crumb ) {
            $markup['itemListElement'][ $key ] = array(
                '@type'    => 'ListItem',
                'position' => $key + 1,
                'item'     => array(
                    'name' => $crumb[0],
                ),
            );
            if ( ! empty( $crumb[1] ) ) {
                $markup['itemListElement'][ $key ]['item'] += array( '@id' => $crumb[1] );
            } elseif ( isset( $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'] ) ) {
                $current_url = set_url_scheme( 'http://' . wp_unslash( $_SERVER['HTTP_HOST'] ) . wp_unslash( $_SERVER['REQUEST_URI'] ) ); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
                $markup['itemListElement'][ $key ]['item'] += array( '@id' => $current_url );
            }
        }
        echo '<script type="application/ld+json">';
        echo json_encode($markup);
        echo '</script>';
    }
}

// Add social profile links in organisation schema start
// https://furnitureroots.com/
add_action('wp_head', 'kp_custom_schema_for_organization');
function kp_custom_schema_for_organization(){
    if(is_front_page()){
        // var_dump('<pre>');
        echo '<script type="application/ld+json">
        {"@context" : "http://schema.org",
            "@type" : "Organization",
            "name" : "FurnitureRoots",
            "url" : "https://furnitureroots.com/",
            "sameAs" : [ "https://linkedin.com/company/furnitureroots",
            "https://www.instagram.com/furnitureroots_official/",
            "https://www.facebook.com/furnitureroots/",
            "https://in.pinterest.com/furnitureroots/"]
        }
        </script>';
    }
}
//Add social profile links in organisation schema end